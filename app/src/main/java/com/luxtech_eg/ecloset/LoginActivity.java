package com.luxtech_eg.ecloset;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity  {
    String TAG=LoginActivity.class.getSimpleName();

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */


    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.

        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.Login_button || id == EditorInfo.IME_NULL) {

                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.Login_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO make add user name and pasword saba7 el validation
                if(mEmailView.getText().toString().equals("Abcd@gmail.com")&& mPasswordView.getText().toString().equals("1234")){
                    String temp =getIntent().getExtras().getString(getString(R.string.temp_key),"");
                    Intent mainIntent = new Intent (LoginActivity.this,Main2Activity.class);
                    mainIntent.putExtra(getString(R.string.temp_key),temp);
                    startActivity(mainIntent);
                    LoginActivity.this.finish();
                } else{
                    Toast.makeText(LoginActivity.this,"Wrong username or password. ",Toast.LENGTH_LONG).show();
                }
            }
        });

        //mLoginFormView = findViewById(R.id.login_form);
       // mProgressView = findViewById(R.id.login_progress);
        /*ArrayList<String> namesOfEvents=CalendarUtility.readCalendarEvent(LoginActivity.this);
        for (int i = 0; i < namesOfEvents.size() ; i++) {
                Log.v("event name",""+namesOfEvents.get(i));
        }*/
        Event nextEvent=CalendarUtility.getFirstUpcomingEvent(LoginActivity.this);
        Log.v(TAG, "nextEvent" + nextEvent.getHomeUpcomimgEvent());
    }





}

