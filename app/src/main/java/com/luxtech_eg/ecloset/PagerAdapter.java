package com.luxtech_eg.ecloset;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.luxtech_eg.ecloset.Slides._02_2_IntroSlider_1_Class;
import com.luxtech_eg.ecloset.Slides._02_3_IntroSlider_2_Class;
import com.luxtech_eg.ecloset.Slides._02_4_IntroSlider_3_Class;
import com.luxtech_eg.ecloset.Slides._02_5_IntroSlider_4_Class;


public class PagerAdapter extends FragmentPagerAdapter {

    public PagerAdapter(FragmentManager fm) {
        super(fm);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Fragment getItem(int arg0) {
        // TODO Auto-generated method stub
        switch (arg0) {
            case 0:
                return new _02_2_IntroSlider_1_Class();

            case 1:
                return new _02_3_IntroSlider_2_Class();

            case 2:
                return new _02_4_IntroSlider_3_Class();

            case 3:
                return new _02_5_IntroSlider_4_Class();

            default:
                break;
        }
        return null;
    }

    // This method to return the number of the page in the slide
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return 4;
    }

}
