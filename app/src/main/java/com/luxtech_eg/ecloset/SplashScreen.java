package com.luxtech_eg.ecloset;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class SplashScreen extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    int DELAY_TIME = 2000;
    String TAG = SplashScreen.class.getSimpleName();
    GoogleApiClient mGoogleApiClient;
    LocationManager locationManager;
    private static final int TWO_MINUTES = 1000 * 60 * 2;
    int GPS_REQUEST_CODE=1000;
    double defaultLocationLon=31.205753;
    double defaultLocationLat=29.924526;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v(TAG, "onActivityResult request code " + requestCode);
        Log.v(TAG,"onActivityResult result code "+resultCode);
        if(requestCode== GPS_REQUEST_CODE){
            if (resultCode==-1){
                //success
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                delayUntillYouHaveInternet();
            }
            else {
                Toast.makeText(getApplication(), "You have to turn on Location settings ", Toast.LENGTH_LONG).show();

            }
        }

    }

    void delayUntillYouHaveInternet() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(Connectivity.isConnected(getApplicationContext())) {
                    if (ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    Location lastKnownGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    Location lastKnownNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    Location lastKnownPassive = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                    Location bestLocation = getBestLocation(lastKnownGPS, lastKnownNetwork, lastKnownPassive);
                    if(bestLocation== null){
                        Log.v(TAG,"using Egypt as Default location");
                         new FetchWeatherTask("" + defaultLocationLat, "" + defaultLocationLon).execute();
                    }else {
                        Log.v(TAG, "best Location lat :" + bestLocation.getLatitude() + " best Location lon :" + bestLocation.getLongitude());
                        new FetchWeatherTask("" + bestLocation.getLatitude(), "" + bestLocation.getLongitude()).execute();

                    }
                }else {
                    delayUntillYouHaveInternet();
                }
            }
        }, DELAY_TIME);
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.v(TAG,"onResume()");

        mGoogleApiClient = new GoogleApiClient.Builder(SplashScreen.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        /**** the first call ****/
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.v(TAG, "SUCCESS");
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        delayUntillYouHaveInternet();

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.v(TAG, "RESOLUTION_REQUIRED");
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(SplashScreen.this, GPS_REQUEST_CODE);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.v(TAG, "SETTINGS_CHANGE_UNAVAILABLE");
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        Toast.makeText(getApplication(), "settings change is unAvailable ", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.v(TAG,"onConnected");

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v(TAG,"onConnectionSuspended");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.v(TAG,"onConnectionFailed");

    }
    void startLoginActivity(String temp){
        Intent i = new Intent(SplashScreen.this, LoginActivity.class);
        i.putExtra(getString(R.string.temp_key),temp);
        startActivity(i);
        SplashScreen.this.finish();
    }

    class FetchWeatherTask extends AsyncTask<String, Void, String> {
        String APIKEY="a0413c27202af17b4779f5e0c1a296eb";
        String lat;
        String lon;
        FetchWeatherTask(String lat,String lon){
            this.lat=lat;
            this.lon=lon;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            startLoginActivity(s);
        }

        @Override
        protected String doInBackground(String... params) {
            // These two need to be declared outside the try/catch
            // so that they can be closed in the finally block.
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;


            // Will contain the raw JSON response as a string.
            String forecastJsonStr = null;

            try {
                // Construct the URL for the OpenWeatherMap query
                // Possible parameters are available at OWM's forecast API page, at
                // http://openweathermap.org/API#forecast

                //old url
                //http://api.openweathermap.org/data/2.5/weather?mode=json&units=metric&lat=35&lon=139&appid=a0413c27202af17b4779f5e0c1a296eb

                Uri.Builder builder = new Uri.Builder();
                builder.scheme("http").authority("api.openweathermap.org")
                        .appendPath("data")
                        .appendPath("2.5")
                        .appendPath("weather")
                        .appendQueryParameter("mode", "json")
                        .appendQueryParameter("units", "metric")
                        .appendQueryParameter("lat", lat)
                        .appendQueryParameter("lon", lon )
                        .appendQueryParameter("appid", APIKEY);
                URL url= new URL(builder.build().toString());
                Log.v(TAG,url.toString());

                // Create the request to OpenWeatherMap, and open the connection
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    forecastJsonStr = null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    forecastJsonStr = null;
                }
                forecastJsonStr = buffer.toString();
                Log.v(TAG,"weather string "+forecastJsonStr);
                return getTempFromJson(forecastJsonStr);
            } catch (IOException e) {
                Log.e("PlaceholderFragment", "Error ", e);
                // If the code didn't successfully get the weather data, there's no point in attempting
                // to parse it.
                forecastJsonStr = null;
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("PlaceholderFragment", "Error parcing json ", e);
            } finally{
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e("PlaceholderFragment", "Error closing stream", e);
                    }
                }
            }
            return "";
        }
    }
    String getTempFromJson(String forecastJsonStr){
        String MAIN="main";
        String TEMP="temp";
        String temp="";
        try {
            JSONObject forecastJson = new JSONObject(forecastJsonStr);
            JSONObject mainObj = forecastJson.getJSONObject(MAIN);
            temp=mainObj.getString(TEMP);
            Log.v(TAG,"temp"+temp+"\u2103");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return temp;
    }
    Location getBestLocation(Location gps, Location network,Location passive){
        // TODO initialize with alexandria
        Location bestLocation= gps;
        if( isBetterLocation(gps,network)){
            bestLocation=gps;
        }else {
            bestLocation= network;
        }

        if (isBetterLocation(bestLocation,passive)){
            bestLocation=bestLocation;
        } else {
            bestLocation =passive;
        }
        return bestLocation;
    }
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        } else if(location==null){
            return false;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }
}
