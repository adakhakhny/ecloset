package com.luxtech_eg.ecloset;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by ahmed on 22/04/16.
 */
public class Event {
    String name;
    String location;
    long beginTime;
    Event(String name, String location, long beginTime){
        this.name= name;
        this.location=location;
        this.beginTime=beginTime;
    }



    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public long getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(long beginTime) {
        this.beginTime = beginTime;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    String getHomeUpcomimgEvent(){
        String returnText ="you have no upcoming events next 24 hours";
        if(name!=null) {
            returnText = "the Next event is" + " " + getName();
            if (beginTime>0){
                returnText = returnText + " " + "at " + getTime( beginTime);
            }
            if (location != null) {
                returnText = returnText + " " + "at " + location;

            }

        }
        Log.v("Event", "return text "+returnText);
        return returnText;
    }
    public static String getTime(long milliSeconds) {
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
}
