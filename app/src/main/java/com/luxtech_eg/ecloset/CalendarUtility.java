package com.luxtech_eg.ecloset;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by ahmed on 22/04/16.
 */
public class CalendarUtility {
    static String TAG =CalendarUtility.class.getSimpleName();
    public static ArrayList<String> nameOfEvent = new ArrayList<String>();
    public static ArrayList<String> startDates = new ArrayList<String>();
    public static ArrayList<String> endDates = new ArrayList<String>();
    public static ArrayList<String> descriptions = new ArrayList<String>();

    public static ArrayList<String> readCalendarEvent(Context context) {
        Cursor cursor = context.getContentResolver()
                .query(
                        Uri.parse("content://com.android.calendar/events"),
                        new String[]{"calendar_id", "title", "description",
                                "dtstart", "dtend", "eventLocation"}, null,
                        null, null);
        cursor.moveToFirst();
        // fetching calendars name
        String CNames[] = new String[cursor.getCount()];

        // fetching calendars id
        nameOfEvent.clear();
        startDates.clear();
        endDates.clear();
        descriptions.clear();

        Log.v(TAG, "cursor size "+cursor.getCount());
        for (int i = 0; i < CNames.length; i++) {
            Log.v(TAG,cursor.getString(1));
            nameOfEvent.add(cursor.getString(1));
            startDates.add(getDate(Long.parseLong(cursor.getString(3))));
            //endDates.add(getDate(Long.parseLong(cursor.getString(4))));
            descriptions.add(cursor.getString(2));
            CNames[i] = cursor.getString(1);
            cursor.moveToNext();

        }
        return nameOfEvent;
    }
    // a function that returns the first upcomming event name
    public static Event getFirstUpcomingEvent(Context context){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        Log.v(TAG,"current time"+dateFormat.format(cal.getTime()));
        long now=cal.getTimeInMillis();
        long dayMilis=24*60*60*1000;
        long end= now+dayMilis;
        String where= "dtstart>"+now+" "+ " AND dtend <"+end;
            String orderBy =  " dtstart "+ " ASC";
        Cursor cursor = context.getContentResolver()
                .query( Uri.parse("content://com.android.calendar/events"),
                        new String[]{"calendar_id", "title", "description",
                                "dtstart", "dtend", "eventLocation"}, where,
                        null, orderBy);

        cursor.moveToFirst();
        /**for Testing **/
        for (int i = 0; i < cursor.getCount(); i++) {
            Log.v(TAG, "upcomming events names"+cursor.getString(1) + " DATE ="+getDate(Long.parseLong(cursor.getString(3))));
            nameOfEvent.add(cursor.getString(1));
            startDates.add(getDate(Long.parseLong(cursor.getString(3))));
            cursor.moveToNext();
        }
        cursor.moveToFirst();
        Log.v(TAG, "cursor size " + cursor.getCount());
        if (cursor.getCount()>0){
            String name=cursor.getString(1);
            String location=cursor.getString(5);
            Log.v(TAG, "Location"+location);
            long beginTime=Long.parseLong(cursor.getString(3));


            return new Event(name,location,beginTime);
        }else {
            return new Event(null,null,0);
        }

    }

    public static String getDate(long milliSeconds) {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "dd/MM/yyyy hh:mm:ss a");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }


}