package com.luxtech_eg.ecloset;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by ahmed on 25/04/16.
 */
public class Connectivity {
    public static boolean isConnected(Context context) {
        boolean isConnected;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        if(!isConnected){
            notConnectedBehavior(context);
        }
        return isConnected;
    }
    public static void notConnectedBehavior(Context context){
        Toast.makeText(context, "no internet connection", Toast.LENGTH_LONG).show();
    }
}