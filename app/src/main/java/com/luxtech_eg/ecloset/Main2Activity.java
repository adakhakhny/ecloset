package com.luxtech_eg.ecloset;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class Main2Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    String TAG = Main2Activity.class.getSimpleName();

    AlertDialog guidanceDialog;
    AlertDialog typeOfImageDialog;
    ImageView topsIV, bottomIV;
    TextView eventText, tempNow;
    Spinner spinner;
    Button tryOtherClothes;
    GoogleApiClient mGoogleApiClient;
    final int TOPS_CASUAL = 0;
    final int TOPS_FORMAL = 1;
    final int BOTTOMS_CASUAL = 2;
    final int BOTTOMS_FORMAL = 3;
    final int TOPS_SPORTS = 4;
    final int BOTTOMS_SPORTS = 5;


    final int SPINNER_POSITION_CASUAL = 0;
    final int SPINNER_POSITION_FORMAL = 1;
    final int SPINNER_POSITION_SPORTS = 2;
    LocationManager locationManager;
    LocationListener locationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        eventText = (TextView) findViewById(R.id.tv_upcoming_event);
        topsIV = (ImageView) findViewById(R.id.iv_top);
        bottomIV = (ImageView) findViewById(R.id.iv_bottom);
        spinner = (Spinner) findViewById(R.id.s_clothes_type);
        tryOtherClothes = (Button) findViewById(R.id.random_search_button);
        tempNow = (TextView) findViewById(R.id.tv_temp);


        /***spinner stuff***/

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.clothes_array, R.layout.spinner_list_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                Log.v(TAG, "on item selected in spinner  " + pos);
                //pos =0 casual
                //pos =1 formal
                //spinnerPosition = pos;
                setImages(pos);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        /***end of spinner stuff***/
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // initializing the alert dialog
        guidanceDialog = new AlertDialog.Builder(this)
                .setTitle("Guidance")
                .setMessage("this is how to use the app simply 1 2 3")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        Log.v(TAG, "ok button clicked");
                        dialog.dismiss();
                    }
                })
                .create();
        String[] types = {"Tops Casual", "Tops Formal", "Bottoms Casual", "Bottoms Formal","Tops Sports", "Bottoms Sports"};
        typeOfImageDialog = new AlertDialog.Builder(this)
                .setTitle("Add Clothes")
                .setItems(types, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        switch (which) {
                            case 0:
                                startCamera(TOPS_CASUAL);
                                break;
                            case 1:
                                startCamera(TOPS_FORMAL);
                                break;
                            case 2:
                                startCamera(BOTTOMS_CASUAL);
                                break;
                            case 3:
                                startCamera(BOTTOMS_FORMAL);
                                break;

                            case 4:
                                startCamera(TOPS_SPORTS);
                                break;
                            case 5:
                                startCamera(BOTTOMS_SPORTS);
                                break;

                        }
                    }

                })
                .create();


        setEventTextAndClothesType();

        String temp =getIntent().getExtras().getString(getString(R.string.temp_key),"");
        setTempText(temp);
        tryOtherClothes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setEventTextAndClothesType();
            }
        });
    }
        //TODO toast for no inter net
        //TODO toat no gps


    void setTempText(String temp) {
        tempNow.setText(getString(R.string.toady_temp_part1) + temp + "\u2103");

    }

    void syncCalendar() {
        Toast.makeText(Main2Activity.this, "syncing calendar", Toast.LENGTH_LONG).show();
        setEventTextAndClothesType();
    }

    public void setEventTextAndClothesType() {
        Event nextEvent = CalendarUtility.getFirstUpcomingEvent(Main2Activity.this);
        eventText.setText(nextEvent.getHomeUpcomimgEvent());
        if (nextEvent.getHomeUpcomimgEvent().contains("Meeting")||nextEvent.getHomeUpcomimgEvent().contains("meeting")
                ||nextEvent.getHomeUpcomimgEvent().contains("Party")||nextEvent.getHomeUpcomimgEvent().contains("party")){
            //Meeting or party formal
            Log.v(TAG,"event is formal");
            setImages(SPINNER_POSITION_FORMAL);
        }else if (nextEvent.getHomeUpcomimgEvent().contains("Gym")||nextEvent.getHomeUpcomimgEvent().contains("gym")
                ||nextEvent.getHomeUpcomimgEvent().contains("Aerobics")||nextEvent.getHomeUpcomimgEvent().contains("aerobics")){
            Log.v(TAG,"event is sports");
            setImages(SPINNER_POSITION_SPORTS);
        }else{
            Log.v(TAG,"event is casual");
            setImages(SPINNER_POSITION_CASUAL);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();
        //setImages(spinnerPosition);


    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    void setImages(int pos){
        //TODO makt the function take args
        Log.v(TAG,"setImages");
        File tops=null;
        File bottoms=null;
        int topsType=0;
        int bottomsType=0;
        if(pos==SPINNER_POSITION_CASUAL){
            topsType=TOPS_CASUAL;
            bottomsType=BOTTOMS_CASUAL;
            tops=getRandomPhoto(TOPS_CASUAL);
            bottoms=getRandomPhoto(BOTTOMS_CASUAL);
        }else if(pos==SPINNER_POSITION_FORMAL){
            topsType=TOPS_FORMAL;
            bottomsType=BOTTOMS_FORMAL;
            tops=getRandomPhoto(TOPS_FORMAL);
            bottoms=getRandomPhoto(BOTTOMS_FORMAL);
        }else if(pos==SPINNER_POSITION_SPORTS){
            topsType=TOPS_SPORTS;
            bottomsType=BOTTOMS_SPORTS;
            tops=getRandomPhoto(TOPS_SPORTS);
            bottoms=getRandomPhoto(BOTTOMS_SPORTS);
        }

        if(tops.length()==0|| bottoms.length()==0){
            Log.v(TAG,"tops or bottoms length is zero");
            setImages(pos);
        }else{
            loadTops(tops);
            loadBottoms(bottoms);
        }
    }

    void loadTops(File tops){
        // if tops is null then there is no images  in folder
        if(tops!=null){
            Picasso.with(Main2Activity.this).load(tops).placeholder(R.drawable.n_image).fit().into(topsIV, new Callback() {
                @Override
                public void onSuccess() {
                    Log.v(TAG, "tops file success");
                }

                @Override
                public void onError() {
                    Log.v(TAG, "tops file onError");
                }
            });
        }
    }
    void loadBottoms(File bottoms){

        // if bottoms is null then there is no images  in folder
        if(bottoms!=null) {
            Picasso.with(Main2Activity.this).load(bottoms).placeholder(R.drawable.n_image).fit().into(bottomIV, new Callback() {
                @Override
                public void onSuccess() {
                    Log.v(TAG, "bottoms file success");
                }

                @Override
                public void onError() {
                    Log.v(TAG, "bottoms file onError");
                }
            });
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            //TODO Start on activity result
            typeOfImageDialog.show();
        } else if (id== R.id.nav_sync){
            Log.v(TAG,"Sync Calendar");
            syncCalendar();
        } else if(id== R.id.nav_guide){
            Log.v(TAG,"Guide");
            Intent guideIntent= new Intent(Main2Activity.this,Five_IntroSlider_Activity.class);
            startActivity(guideIntent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    void startCamera(int typeCloth) {
        Log.v(TAG, "add clothes open camera");
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile(typeCloth);
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));

                startActivityForResult(takePictureIntent, typeCloth);
            }
        }
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v(TAG, "request code was " + requestCode);
    }

    private File createImageFile(int clothType) throws IOException {
        // Create an image file name

        String subDirectory ="unknown";
        switch (clothType){
            case TOPS_CASUAL:
                subDirectory="topscasual";
                break;
            case TOPS_FORMAL:
                subDirectory="topsformal";
                break;
            case BOTTOMS_CASUAL:
                subDirectory="bottomsscasual";
                break;
            case BOTTOMS_FORMAL:
                subDirectory="bottomsformal";
                break;
            case TOPS_SPORTS:
                subDirectory="topssports";
                break;
            case BOTTOMS_SPORTS:
                subDirectory="bottomssports";
                break;
        }
        String mCurrentPhotoPath;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStorageDirectory()+"/"+subDirectory);
        storageDir.mkdirs();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        //Toast.makeText(Main2Activity.this, "saved at "+mCurrentPhotoPath,Toast.LENGTH_LONG).show();
        //Toast.makeText(Main2Activity.this, "size"+image.length(),Toast.LENGTH_LONG).show();

        return image;
    }

    private List<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<File>();
        if (parentDir.listFiles()!=null){
        File[] files = parentDir.listFiles();

        for (File file : files) {
            if (file.isDirectory()) {
                inFiles.addAll(getListFiles(file));
            } else {

                inFiles.add(file);

            }
        }
        }

        return inFiles;
    }
    File getRandomPhoto(int clothType){
        Random randomGenerator = new Random();
        if(getListFiles(new File(getPhotosDirectory(clothType)))!=null) {
            List<File> topsCasualFiles = getListFiles(new File(getPhotosDirectory(clothType)));
            if (topsCasualFiles!=null){


                if (topsCasualFiles.size() > 0) {

                    int randomInt = randomGenerator.nextInt(topsCasualFiles.size());
                    Log.v(TAG, "the random number is " + randomInt);
                    return topsCasualFiles.get(randomInt);
                } else {
                    return null;
                }
            }

        }else{
            return null;
        }
        return null;
    }


    String getPhotosDirectory(int clothType){
        String subDirectory ="unknown";
        switch (clothType){
            case TOPS_CASUAL:
                subDirectory="topscasual";
                break;
            case TOPS_FORMAL:
                subDirectory="topsformal";
                break;
            case BOTTOMS_CASUAL:
                subDirectory="bottomsscasual";
                break;
            case BOTTOMS_FORMAL:
                subDirectory="bottomsformal";
                break;
            case TOPS_SPORTS:
                subDirectory="topssports";
                break;
            case BOTTOMS_SPORTS:
                subDirectory="bottomssports";
                break;
        }
        return Environment.getExternalStorageDirectory()+"/"+subDirectory;

    }


    void printFileNames(List<File> list){
        for (int i = 0; i < list.size(); i++) {
            Log.v(TAG,"filename :"+list.get(i).getName());
        }
    }



}