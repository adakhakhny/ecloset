package com.luxtech_eg.ecloset;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.Toast;

@SuppressLint("NewApi") public class Five_IntroSlider_Activity extends FragmentActivity {

	ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro_slider_activity);
        Log.i("Five Activity Log Info","Intro Slider Activity Working.");
 			Toast.makeText(getApplicationContext(), "Hello Intro Slider Activity Working", Toast.LENGTH_SHORT).show();
 			viewPager = (ViewPager)findViewById(R.id.pager);
 			PagerAdapter padapter = new com.luxtech_eg.ecloset.PagerAdapter(getSupportFragmentManager());
 			//PagerAdapter padapter = new PagerAdapter(getSupportFragmentManager());
 			viewPager.setAdapter(padapter);
 	 	Log.i("Five Activity Log Info","Intro Sliderp Activity Stopped.");

    }
}
